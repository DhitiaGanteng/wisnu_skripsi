$('#user-validation').bootstrapValidator({
        fields: {
            nama_lengkap: {
                validators: {
                    notEmpty: {
                        message: 'Nama Lengkap tidak boleh kosong.'
                    }
                }
            },
             username: {
                validators: {
                    notEmpty: {
                        message: 'Username tidak boleh kosong.'
                    }
                }
            },
            message: {
                validators: {
                    notEmpty: {
                        message: 'The field is required and cannot be empty'
                    }
                }
            },
            password: {
                validators: {

                    notEmpty: {
                        message: 'Password tidak boleh kosong'
                    }
                }
            },
            ulangi_password: {
                validators: {
                    notEmpty: {
                        message: 'Ulangi Password tidak boleh kosong.'
                    },
                    identical: {
                        field: 'password',
                        message: 'Password tidak sesuai dengan password pertama.'
                    }
                }
            },

            password_baru: {
                validators: {
                    notEmpty: {
                        message: 'Password Baru tidak boleh kosong.'
                    }
                }
            }
        },
        submitHandler: function (validator, form, submitButton) {
            console.log(submitButton);
        }
    });