$('#form_pelaku_validation').bootstrapValidator({
        fields: {
            nama_lengkap: {
                validators: {
                    notEmpty: {
                        message: 'Nama lengkap pelaku tidak boleh kosong.'
                    }
                }
            },
             jenis_kelamin: {
                validators: {
                    notEmpty: {
                        message: 'Jenis kelamin tidak boleh kosong.'
                    }
                }
            },
            tempat_lahir: {
                validators: {
                    notEmpty: {
                        message: 'Tempat lahir tidak boleh kosong.'
                    }
                }
            },
            tanggal_lahir: {
                validators: {
                    notEmpty: {
                        message: 'Tanggal lahir tidak boleh kosong.'
                    }
                }
            },
            alamat: {
                validators: {
                    notEmpty: {
                        message: 'Alamat tidak boleh kosong.'
                    }
                }
            },

            alamat_tkp: {
                validators: {
                    notEmpty: {
                        message: 'Alamat TKP tidak boleh kosong.'
                    }
                }
            },
             kecamatan_tkp: {
                validators: {
                    notEmpty: {
                        message: 'Kecamatan TKP tidak boleh kosong.'
                    }
                }
            },
             tgl_penangkapan: {
                validators: {
                    notEmpty: {
                        message: 'Tanggal Penangkapan tidak boleh kosong.'
                    }
                }
            }, 
            vonis_hukuman: {
                validators: {
                    notEmpty: {
                        message: 'Vonis hukuman tidak boleh kosong.'
                    }
                }
            }
        },
        submitHandler: function (validator, form, submitButton) {
            console.log(submitButton);
        }
    });