<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    protected $login_url = "_admin/admin/login"; 

    public function __construct() {
  
        parent::__construct();
    
       $this->session->set_userdata(array(
                            'last_visited' => time()
                    ));
    }

      public function load_view_login($subview){
        $this->data['subview'] = $subview;
        $this->load->view('layouts/login_layout',$this->data);
      }

      public function load_view($data){
        $this->load->view('layouts/backend_layout',$data);    
      }

    

}
