<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Crud_m extends CI_Model {

    public $user_validation = array(
         array('field' => 'c_password', 'label' => 'Confirmation','rules' => 'required'),
         array('field' => 'password', 'label' => 'Password','rules' => 'required'),
         array('field' => 'email', 'label' => 'Email', 'rules' => 'required|is_unique[users.email]')
        );

    public function __construct() {
        parent::__construct();
    }


    public function get_all($table){
        return $this->db->get($table)->result();
    }

    public function get_by($id,$params,$table){
        return $this->db->where($params,$id)->get($table)->row();
    }

    public function insert($table, $data) {
       return $insertSQL = $this->db->insert($table, $data);
    }

    public function update($table, $content, $selection) {
        $updateSQL = $this->db->where($selection, $content[$selection]);
       return $updateSQL = $this->db->update($table, $content);
       
    }

    public function findJoin($table, $table_2, $id, $id_2){
        return $this->db->query("SELECT * FROM $table a JOIN $table_2 b on($id = $id_2)")->result();
    }

    public function findJoinBy($table, $table_2, $id, $id_2, $selection){
        return $this->db->query("SELECT * FROM $table a JOIN $table_2 b on($id = $id_2) WHERE $selection = $")->row();
    }

    

    public function delete($table, $selection1, $id1, $selection2 = null, $id2 = null) {
        $deleteSQL = $this->db->where($selection1, $id1);
        if ($selection2 != null && $id2 != null) {
            $deleteSQL = $this->db->where($selection2, $id2);
        }
            return $deleteSQL = $this->db->delete($table);
    }

    public function json_autocomplete($search, $table, $column) {
        $sql = "SELECT * FROM $table WHERE $column LIKE '%$search%'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function yuk_upload($name) {

 
        // $folder = !empty($path) ? "$path/" : null;
        $config['upload_path'] = './foto_pelaku/';
        $config['allowed_types'] = 'gif|jpg|png';
        $this->upload->initialize($config);
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($name)) {
         
            return FALSE;
        } else {
            $this->upload->data();
        
            return TRUE;
        }
    }

    public function uploado($fupload_name, $var = null, $width = null) {
        $vdir_upload = "./assets/company_photos/";
        $vfile_upload = $vdir_upload . $fupload_name;
        $variable = empty($var) ? 'foto' : $var;
        $s = move_uploaded_file($_FILES["$variable"]["tmp_name"], $vfile_upload);
        $tipe_file = $_FILES["$variable"]['type'];
        if ($tipe_file == "image/jpg") {
            $im_src = imagecreatefromjpeg($vfile_upload);
            $src_width = imageSX($im_src);
            $src_height = imageSY($im_src);
            $dst_width = isset($width) ? $width : 505;
            $dst_height = ($dst_width / $src_width) * $src_height;
            $im = imagecreatetruecolor($dst_width, $dst_height);
            imagecopyresampled($im, $im_src, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);
            $nm = imagejpeg($im, $vdir_upload . "" . $fupload_name);
            imagedestroy($im_src);
            imagedestroy($im);
        }
        if ($s == 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function unlink($file, $path = null) {
        $folder = isset($path) ? "$path/" : null;
        $cek_file = file_exists("assets/member_photos/$file");
        if ($cek_file) {
            unlink("assets/member_photos/$file");
            return true;
        }
    }

    // public function getAllUsers(){
    //     return $this->db->query("SELECT DISTINCT id, nama_lengkap, username, active  FROM users")->result();
    // }
    public function getAllUsers(){
        return $this->db->query("SELECT DISTINCT u.id, u.nama_lengkap, u.username,g.description, u.active  FROM users u JOIN users_groups ug on(u.id = ug.user_id) JOIN groups g on(ug.group_id = g.id)")->result();
    }
    public function getAllUsersById($id){
        return $this->db->query("SELECT DISTINCT u.id, u.nama_lengkap, u.username,g.description,g.id as id_group, u.active  FROM users u JOIN users_groups ug on(u.id = ug.user_id) JOIN groups g on(ug.group_id = g.id) WHERE u.id = $id")->row();
    }

    public function updateGroup($id, $group_id){
        $this->db->where('user_id',$id);
        $update = $this->db->update('users_groups',array('group_id'=>$group_id));
        if($update){
           return true;
        }

        return false;

    }

    public function total_pelaku(){
        return $this->db->query("SELECT * from data_pelaku")->num_rows();
    }

   public function getMapData(){
    return $this->db->query("SELECT k.kecamatan as name, count(d.id) as value from data_pelaku d RIGHT JOIN kecamatan k on(d.kecamatan_tkp = k.kecamatan) group by k.kecamatan")->result();
   }
}
