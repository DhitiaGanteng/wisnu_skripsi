
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title><?= isset($header) ? $header : ''?>   </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="shortcut icon" href="img/favicon.ico"/>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <!-- global css -->
    <link type="text/css" href="<?= site_url('assets/css/app.css') ?>" rel="stylesheet"/>
    <link type="text/css" href="<?= site_url('assets/css/custom.css') ?>" rel="stylesheet"/>
    <link type="text/css" href="<?= site_url('assets/css/custom_css/skins/skin-default.css') ?>" rel="stylesheet" id="skin"/>

    <link rel="stylesheet" type="text/css" href="<?= site_url('assets/vendors/datatables/css/dataTables.bootstrap.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?= site_url('assets/css/custom.css') ?>">

    <link rel="stylesheet" type="text/css" href="<?= site_url('assets/css/custom_css/datatables_custom.css') ?>">
    <!--end of page level css-->
      <link href="<?= site_url('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') ?>" rel="stylesheet">
    <!-- end of global css -->
      <link rel="stylesheet" type="text/css" href="<?= site_url('assets/vendors/sweetalert2/css/sweetalert2.min.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?= site_url('assets/css/custom_css/sweet_alert2.css') ?>">
    <!-- global js -->
<script src="<?= site_url('assets/js/app.js') ?>" type="text/javascript"></script>
</head>

<body class="skin-default">
<div class="preloader">
    <div class="loader_img"><img src="<?= site_url('assets/img/loader.gif') ?>" alt="loading..." height="64" width="64"></div>
</div>
<!-- header logo: style can be found in header-->
<header class="header">
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="index.html" class="logo">
            <!-- Add the class icon to your logo image or logo icon to add the margining -->
            <!-- <img src="<?= site_url('assets/img/logo.png') ?>" alt="logo"/> -->
        </a>
        <!-- Header Navbar: style can be found in header-->
        <!-- Sidebar toggle button-->
        <!-- Sidebar toggle button-->
        <div>
            <a href="javascript:void(0)" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button"> <i
                    class="fa fa-fw ti-menu"></i>
            </a>
        </div>
        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown-->
                <li class="dropdown user user-menu">
                    <a href="javascript:void(0)" class="dropdown-toggle padding-user" data-toggle="dropdown">
                      
                        <div class="riot">
                            <div>
                                <?= $this->ion_auth->user()->row()->nama_lengkap ?>
                                <span>
                                        <i class="caret"></i>
                                    </span>
                            </div>
                        </div>
                    </a>
                    <ul class="dropdown-menu">
                     
                       
                        <li role="presentation" class="divider"></li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            
                            <div >
                                <a href="<?= site_url('Dashboard/Auth/Logout') ?>">
                                
                                    Logout
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar-->
        <section class="sidebar">
            <div id="menu" role="navigation">
                <div class="nav_profile">
                    <div class="media profile-left">
                        <a class="pull-left profile-thumb" href="javascript:void(0)">
                            <img src="<?= site_url('assets/img/lambang_polda.png') ?>" class="img-circle" alt="User Image">
                        </a>
                        <div class="content-profile">
                            <h5 class="text-center">
                                Sistem Informasi Monitoring Tindak Pidana Narkoba <br/><br/> <b>Polres Klaten</b>
                            </h4>
                          
                        </div>
                    </div>
                </div>
                <ul class="navigation">
                    <li class=" <?= (isset($dashboard) ? 'active' : '') ?>">
                        <a href="<?= site_url('') ?>">
                            <i class="menu-icon ti-home"></i>
                            <span class="mm-text ">Dashboard</span>
                        </a>
                    </li>
                
                 <?php if($this->ion_auth->is_admin()){ ?>
                    <li class=" <?= (isset($admin) ? 'active' : '') ?>">
                        <a href="<?= site_url('Dashboard/Auth/admin_list') ?>">
                            <i class="menu-icon ti-user"></i>
                            <span>Users</span> 
                        </a>
                        
                    </li>
                <?php } ?>
                    <li class="menu-dropdown <?= (isset($pelaku) ? 'active' : '') ?>">
                        <a href="javascript:void(0)">
                            <i class="menu-icon ti-check-box"></i> Pelaku
                            <span class="fa arrow"></span>
                        </a>
                         <ul class="sub-menu form-submenu">
                             <?php if($this->ion_auth->is_admin()){ ?>
                                    <li class="<?= (isset($form_pelaku) ? 'active' : '') ?>">
                                        <a href="<?= site_url('Dashboard/Pencatatan/form_pelaku') ?>">
                                            <i class="fa fa-pencil-square-o"></i> Form Pelaku
                                        </a>
                                    </li>
                            <?php } ?>
                                    <li class="<?= (isset($list_pelaku) ? 'active' : '') ?>">
                                        <a href="<?= site_url('Dashboard/Pencatatan/data_pelaku') ?>">
                                            <i class="fa fa-list"></i> Data Pelaku
                                        </a>
                                    </li>
                                   
                                    </li>

                                </ul>
                        
                    </li>

                    <li class=" <?= (isset($peta_kejahatan) ? 'active' : '') ?>">
                        <a href="<?= site_url('Dashboard/Peta/narkoba') ?>">
                            <i class="menu-icon ti-location-pin"></i>
                            <span>Peta Kejahatan</span> 
                        </a>
                        
                    </li>
                    
                
                    
                
                </ul>
                <!-- / .navigation -->
            </div>
            <!-- menu -->
        </section>
        <!-- /.sidebar -->
    </aside>
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?= isset($header) ? $header : '' ?></h1>
            
        </section>
        <!-- Main content -->
        <section class="content">

            <!--rightside bar -->
              <?= $this->load->view($subview) ?>
            <div class="background-overlay"></div>
        </section>
        <!-- /.content -->
    </aside>
    <!-- /.right-side -->
</div>
<!-- /.right-side -->
<!-- ./wrapper -->

<!-- end of page level js -->
<!-- begining of page level js -->
<script type="text/javascript" src="<?= site_url('assets/vendors/flip/js/jquery.flip.min.js') ?>"></script>
<script type="text/javascript" src="<?= site_url('assets/vendors/lcswitch/js/lc_switch.min.js') ?>"></script>
<script type="text/javascript" src="<?= site_url('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') ?>"></script>
<script type="text/javascript" src="<?= site_url('assets/vendors/bootstrap-maxlength/js/bootstrap-maxlength.js') ?>"></script>

<script type="text/javascript" src="<?= site_url('assets/vendors/datatables/js/jquery.dataTables.js') ?>"></script>
<script type="text/javascript" src="<?= site_url('assets/vendors/datatables/js/dataTables.bootstrap.js') ?>"></script>
<script type="text/javascript" src="<?= site_url('assets/js/custom_js/datatables_custom.js') ?>"></script>
<!-- end of page level js -->
<script type="text/javascript" src="<?= site_url('assets/js/user_validation.js') ?>"></script>
<script type="text/javascript" src="<?= site_url('assets/js/form_pelaku_validation.js') ?>"></script>
<script type="text/javascript" src="<?= site_url('assets/vendors/sweetalert2/js/sweetalert2.min.js') ?>"></script>

<script src="<?= site_url('assets/vendors/iCheck/js/icheck.js') ?>" type="text/javascript"></script>
<script src="<?= site_url('assets/vendors/datedropper/datedropper.js') ?>" type="text/javascript"></script>
<script type="text/javascript" src="<?= site_url('assets/vendors/moment/js/moment.min.js') ?>"></script>
<script type="text/javascript" src="<?= site_url('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') ?>"></script>
<script type="text/javascript" src="<?= site_url('assets/vendors/select2/js/select2.js') ?>"></script>
<script src="<?= site_url('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') ?>" type="text/javascript"></script>
<script src="<?= site_url('assets/js/custom_js/complex_form2.js') ?>" type="text/javascript"></script>

</body>


</html>
