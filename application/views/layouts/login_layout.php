
<!DOCTYPE html>
<html>

<head>
    <title>::Admin Login::</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="img/favicon.ico"/>
    <!-- Bootstrap -->
    <link href="<?= site_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <!-- end of bootstrap -->
    <!--page level css -->
    <link type="text/css" href="<?= site_url('assets/vendors/themify/css/themify-icons.css') ?>" rel="stylesheet"/>
    <link href="<?= site_url('assets/vendors/iCheck/css/all.css') ?>" rel="stylesheet">
    <link href="<?= site_url('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') ?>" rel="stylesheet"/>
    <link href="<?= site_url('assets/css/login.css') ?>" rel="stylesheet">
    <!--end page level css-->
</head>

<body id="sign-in">
<div class="preloader">
    <div class="loader_img"><img src="<?= site_url('assets/img/loader.gif') ?>" alt="loading..." height="64" width="64"></div>
</div>
   <?= $this->load->view($subview); ?>
<!-- global js -->
<script src="<?= site_url('assets/js/jquery.min.js') ?>" type="text/javascript"></script>
<script src="<?= site_url('assets/js/bootstrap.min.js') ?>" type="text/javascript"></script>
<script src="<?= site_url('assets/vendors/iCheck/js/icheck.js') ?>" type="text/javascript"></script>
<script src="<?= site_url('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') ?>" type="text/javascript"></script>
<script src="<?= site_url('assets/js/custom_js/login.js') ?>" type="text/javascript"></script>

</body>

</html>
