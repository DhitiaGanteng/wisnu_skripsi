      <link href="<?= site_url('assets/vendors/iCheck/css/all.css') ?>" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="<?= site_url('assets/vendors/gridforms/css/gridforms.css') ?>"/>
     <link href="<?= site_url('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') ?>" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" type="text/css" href="<?= site_url('assets/css/custom_css/complex_forms2.css'); ?>"/>
     <link rel="stylesheet" type="text/css" href="<?= site_url('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?= site_url('assets/vendors/select2/css/select2.min.css') ?>">
  <section class="content">
 <div class="row" id="complex-form2">
                <!--5th tab bank application starting-->
                <div class="col-lg-12">
                   
                <?= form_open_multipart('Dashboard/Pencatatan/proses_form', array('id' => 'form_pelaku_validation','class'=> 'grid-form form-horizontal bv-form', 'novalidate' => 'novalidate')) ?>
                        <div class="text-center">
                            
                            <h3>Detail Pelaku Tindak Pidana Narkoba</h3>

                        </div>

<br><br>
                  		
                        <br/>
                        <br/>
                        <fieldset>
                            <legend><i class="fa fa-user"></i> Biodata Diri</legend>
                            <div data-row-span="4">
                                <div data-field-span="3">
                                 
                                    <div class="form-group">
                                        <label for="first_appl" class="col-md-3">Nama Pelaku</label>
                                            : <?= $row_pelaku->nama_lengkap ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="second_appl" class="col-md-3">Jenis Kelamin</label>
                      
                                  
                                        <?php if($row_pelaku->jenis_kelamin == 'laki-laki'){
                                        	?>
                                        	: Laki - Laki
                                        	<?php
                                        	}else{
                                        		?>
                                        		: Perempuan
                                        		<?php
                                        		
                                        		} ?>
                                
                                      
                                     
                                      
                                    </div>
                                    <div class="form-group">
                                        <label for="first_appl" class="col-md-3">Tempat Lahir</label>
                                        
                                             : <?= $row_pelaku->tempat_lahir ?>
                                      
                                    </div>
                                    <div class="form-group">
                                        <label for="third_appl" class="col-md-3">Tanggal Lahir</label>
                                        : <?= $row_pelaku->tanggal_lahir ?>
                                    </div>

                                     
                                    <div class="form-group">
                                        <label for="first_appl" class="col-md-3">Alamat</label>
                                        : <?= $row_pelaku->alamat ?>
                                    </div>
                                    
                                </div>
                                <div data-field-span="1">
                                    <div class="text-center" style="padding-top:22px;">
                  			<?php if($row_pelaku->foto != ''){
                                ?>
                                 <img src="<?= site_url('foto_pelaku/'.$row_pelaku->foto) ?>" height="150" class="" alt="User Image">
                                <?php
                                }else{
                                    ?>
                                      <img src="<?= site_url('foto_pelaku/default_pp.png') ?>" height="150" class="" alt="User Image">
                                    <?php
                                    } ?>

                  		</div>
                                </div>

                                
                            </div>
                        
                        </fieldset>
                        <br>
                        <br>
                       
                        <fieldset>
                        <legend><i class="fa fa-chain"></i> Detail Informasi Penangkapan Pelaku</legend>
                            <div data-row-span="4">
                                <div data-field-span="4">
                                 <br>
                                <div class="form-group">
                                        <label for="first_appl" class="col-md-3">Alamat TKP</label>
                                       : <?= $row_pelaku->alamat_tkp ?>
                                </div>
                                <div class="form-group">
                                        <label for="first_appl" class="col-md-3">Kecamatan TKP</label>
                                        : <?= $row_pelaku->kecamatan_tkp ?>
                                </div>
                                <div class="form-group">
                                        <label for="first_appl" class="col-md-3">Tanggal Penangkapan</label>
                                      : <?= $row_pelaku->tgl_penangkapan ?>
                                </div>
                                <div class="form-group">
                                        <label for="first_appl" class="col-md-3">Vonis Hukuman</label>
                                       : <?= $row_pelaku->vonis_hukuman ?>
                                </div>
                                 <div class="form-group">
                                        <label for="first_appl" class="col-md-3">Catatan</label>
                                       : <?= $row_pelaku->catatan ?>
                                </div>

                               
                                </div>
                            </div>
                        
                        </fieldset>
                  
                        
                    
                    </form>
                       <div data-row-span="4">
                       <br><br>
                                <div data-field-span="4">
                                 <?php if($this->ion_auth->is_admin()){ ?>
                                <a href="<?= site_url('Dashboard/Pencatatan/form_pelaku/'.$row_pelaku->id) ?>">  <button type="submit" class="btn btn-primary"><i class="fa fa-paper-plane"></i> Ubah</button></a>
                                <?php } ?>
                                <a href="<?= site_url('Dashboard/Pencatatan/data_pelaku') ?>"> <button type="button" class="btn btn-default"><i class="fa fa-long-arrow-left"></i> Cancel</button></a>
                               
                                </div>
                        </div>

                </div>
            </div>
             <br><br><br>
         
            <div class="background-overlay"></div>
            </section>


