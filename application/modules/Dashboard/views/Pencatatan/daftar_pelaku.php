<div class="row">
			 <?php if(!is_null($this->session->flashdata('success'))){ ?>

              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fa fa-check"></i> <?= $this->session->flashdata('success') ?></h5>
               		
              </div>
			        
			   <?php  } ?>   
</div>
<div class="row">
                <div class="col-lg-12">
                    <div class="panel ">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="ti-layout-grid3"></i> Daftar Pelaku Tindak Pidana Narkoba
                            </h3>
                            <span class="pull-right">
                                    <i class="fa fa-fw ti-angle-up clickable"></i>
                                  
                                </span>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="sample_1">
                                    <thead>
                                    <tr>
                                        <th>
                                            Nama
                                        </th>
                                        <th>Jenis Kelamin</th>
                                        <th>Kecamatan Penangkapan</th>
                                        <th>Tanggal Penangkapan</th>
                                           
                                        <th>
                                            
                                        </th>
                                     
                                       
                                    </tr>
                                    </thead>
                                    <tbody>
                                   	<?php 
                                   	foreach ($pelaku_data as $key => $value) {
                                   		?>
                                   		<tr>
                                   			<td><?= $value->nama_lengkap ?></td>
                                   			<td><?= $value->jenis_kelamin ?></td>
                                   			<td><?= $value->kecamatan_tkp  ?></td>
                                   			<td><?= $value->tgl_penangkapan ?></td>
                                           
                                   			<td>
                                   				<a href="<?= site_url('Dashboard/Pencatatan/detail_pelaku/'.$value->id)  ?>"><button class="btn btn-info"><i class="fa fa-search"></i></button></a>
                                           <?php if($this->ion_auth->is_admin()){ ?>
                                   				<a href="<?= site_url('Dashboard/Pencatatan/form_pelaku/'.$value->id)  ?>"><button class="btn btn-primary"><i class="fa fa-edit"></i></button></a>
                                        
                                        		<button type="button"  data-id="<?= $value->id ?>" class="btn btn-danger hapus_pelaku"><i class="fa fa-trash"></i></button>
                                             <?php } ?>
                                   			</td>
                                       
                                   		</tr>
                                   		<?php
                                   	}

                                   	 ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>

            <script type="text/javascript">
         

					 $('.hapus_pelaku').on('click', function (e) {
					 		var id = $(this).attr('data-id');

					        swal({
					            title: 'Apakah anda yakin menghapus data pelaku ini?',
					            text: "",
					            type: 'question',
					            showCancelButton: true,
					            confirmButtonColor: '#66cc99',
					            cancelButtonColor: '#ff6666',
					            confirmButtonText: 'Ya, Hapus Pelaku ini.',
					            cancelButtonText: 'Tidak, Batalkan!',
					            confirmButtonClass: 'btn btn-success',
					            cancelButtonClass: 'btn btn-danger'
					        }).then(function () {
					        		
					            swal(
					                'Berhasil Menghapus Data Pelaku',
					                'Pelaku tindak pidana narkoba telah berhasil dihapus.',
					                'success'
					            ).then(function(){
					            	var url = '<?= site_url()?>Dashboard/Pencatatan/delete_pelaku/'+id;
									location.href= url;
					            });
					        }, function (dismiss) {
					            // dismiss can be 'cancel', 'overlay',
					            // 'close', and 'timer'
					            if (dismiss === 'cancel') {
					                swal(
					                    'Dibatalakan.',
					                    'Penghapusan Pelaku tindak pidana narkoba telah dibatalkan.',
					                    'error'
					                );
					            }
					        })
					    });

					 $('#ubah_password').hide();
					  var ubah_password = function(){
					    $('#ubah_password').toggle('slow');
					  }
            </script>
