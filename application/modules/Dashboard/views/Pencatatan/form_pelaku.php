      <link href="<?= site_url('assets/vendors/iCheck/css/all.css') ?>" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="<?= site_url('assets/vendors/gridforms/css/gridforms.css') ?>"/>
     <link href="<?= site_url('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') ?>" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" type="text/css" href="<?= site_url('assets/css/custom_css/complex_forms2.css'); ?>"/>
     <link rel="stylesheet" type="text/css" href="<?= site_url('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?= site_url('assets/vendors/select2/css/select2.min.css') ?>">
  <section class="content">
 <div class="row" id="complex-form2">
                <!--5th tab bank application starting-->
                <div class="col-lg-12">
                   
                <?= form_open_multipart('Dashboard/Pencatatan/proses_form/'.(isset($row) ? $row->id : ''), array('id' => 'form_pelaku_validation','class'=> 'grid-form form-horizontal bv-form', 'novalidate' => 'novalidate')) ?>
                        <div class="text-center">
                             <img src="<?= site_url('assets/img/lambang_polda.png') ?>" height="100" class="" alt="User Image">
                           <?php if(isset($row)){
                            ?>
                            <h3>Ubah Data Pelaku Tindak Pidana Narkoba</h3>
                            <input type="hidden" name="id" value="<?= $row->id ?>">
                            <?php
                            }else{
                                ?>
                                <h3>Form Pengisian Data Pelaku Tindak Pidana Narkoba</h3>
                                <?php
                                } ?> 
                        </div>
                  
                        <fieldset>
                            <legend>(A) Biodata Diri</legend>
                            <div data-row-span="4">
                                <div data-field-span="4">
                                 
                                    <div class="form-group">
                                        <label for="first_appl" class="col-md-2">Nama Pelaku</label>
                                        <div class="col-md-10">
                                            <input class="form-control" value="<?= isset($row) ? $row->nama_lengkap : ''  ?>" name="nama_lengkap" id="first_appl" required type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="second_appl" class="col-md-2">Jenis Kelamin</label>
                                        <div class="col-md-10">
                                            
                                       
                                        <label>
                                            <input type="radio" <?= isset($row) ? $row->jenis_kelamin == 'laki-laki' ? 'checked': '' : ''  ?> required name="jenis_kelamin" value="laki-laki" class="square-blue">&nbsp;Laki - Laki
                                        </label>
                                        <label>
                                            <input type="radio" <?= isset($row) ? $row->jenis_kelamin == 'perempuan' ? 'checked': '' : ''  ?> name="jenis_kelamin" value="perempuan" class="square-blue">&nbsp;Perempuan
                                        </label>
                                     
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="first_appl" class="col-md-2">Tempat Lahir</label>
                                        <div class="col-md-10">
                                             <input type='text' value="<?= isset($row) ? $row->tempat_lahir : ''  ?>"  required name="tempat_lahir" placeholder=" " class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="third_appl" class="col-md-2">Tanggal Lahir</label>
                                        <div class="col-md-10">
                                       
                                            <div class='input-group date'>
                                                <input type='text' value="<?= isset($row) ? $row->tanggal_lahir : ''  ?>"  id="dob_appl1" required name="tanggal_lahir" class="form-control"/>
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                       
                                        </div>
                                    </div>

                                     
                                    <div class="form-group">
                                        <label for="first_appl" class="col-md-2">Alamat</label>
                                        <div class="col-md-10">
                                                <textarea id="permanent_address" required name="alamat" class="resize_vertical" rows="4"><?= isset($row) ? $row->alamat : ''  ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="first_appl" class="col-md-2">Foto</label>
                                        <div class="col-md-10">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput"
                                             style="width: 200px; height: 150px;">
                                                 <?php 
                                                 if(isset($row)){


                                                 if($row->foto != ''){
                                                    ?>
                                                     <img src="<?= site_url('foto_pelaku/'.$row->foto) ?>" height="160" class="" alt="User Image">
                                                    <?php
                                                    }else{
                                                        ?>
                                                          <img src="<?= site_url('foto_pelaku/default_pp.png') ?>" height="150" class="" alt="User Image">
                                                        <?php
                                                        }} ?>
                                             </div>
                                        <div>
                                                <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span
                                                        class="fileinput-exists">Change</span>
                                                <input type="file" name="foto"  >
                                                </span>
                                            <a href="#" class="btn btn-default fileinput-exists"
                                               data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        
                        </fieldset>
                        <br>
                        <br>
                       
                        <fieldset>
                        <legend>(B) Detail Informasi Penangkapan Pelaku</legend>
                            <div data-row-span="4">
                                <div data-field-span="4">
                                 <br>
                                <div class="form-group">
                                        <label for="first_appl" class="col-md-2">Alamat TKP</label>
                                        <div class="col-md-10">
                                             <textarea id="alamat_tkp" required name="alamat_tkp" class="resize_vertical" rows="4"><?= isset($row) ? $row->alamat_tkp : ''  ?></textarea>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <label for="first_appl" class="col-md-2">Kecamatan TKP</label>
                                        <div class="col-md-10">
                                            <select id="country" name="kecamatan_tkp" required>
                                            <option>Pilih Kecamatan</option>
                                           
                                            <option value="kemalang" title="Kemalang">Kemalang</option>
                                            <option value="manisrenggo" title="Manisrenggo">Manisrenggo</option>
                                            <option value="prambanan" title="Prambanan">Prambanan</option>
                                            <option value="karangnongko" title="Karangnongko">Karangnongko</option>
                                            <option value="kebonarum" title="Kebonarum">Kebonarum</option>
                                            <option value="gantiwarno" title="Gantiwarno">Gantiwarno</option>
                                            <option value="jogonalan" title="Jogonalan">Jogonalan</option>
                                            <option value="jatinom" title="Jatinom">Jatinom</option>
                                            <option value="tulung" title="Tulung">Tulung</option>
                                            <option value="polanharjo" title="Polanharjo">Polanharjo</option>
                                            <option value="wonosari" title="Wonosari">Wonosari</option>
                                            <option value="juwiring" title="Juwiring">Juwiring</option>
                                            <option value="karangdowo" title="Karangdowo">Karangdowo</option>
                                            <option value="cawas" title="Cawas">Cawas</option>
                                            <option value="bayat" title="Bayat">Bayat</option>
                                            <option value="wedi" title="Wedi">Wedi</option>
                                            <option value="klaten_selatan" title="Klaten Selatan">Klaten Selatan</option>
                                            <option value="klaten_tengah" title="Klaten Tengah">Klaten Tengah</option>
                                            <option value="Kali Kotes" title="Kali Kotes">Kali Kotes</option>
                                            <option value="ceper" title="Ceper">Ceper</option>
                                            <option value="delanggu" title="Delanggu">Delanggu</option>
                                            <option value="karanganom" title="Karanganom">Karanganom</option>
                                            <option value="ngawen" title="Ngawen">Ngawen</option>
                                            <option value="klaten Utara" title="Klaten Utara">Klaten Utara</option>
                                            <option value="pedan" title="Pedan">Pedan</option>
                                            <option value="trucuk" title="Trucuk">Trucuk</option>
                                        
                                           
                                            </select>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <label for="first_appl" class="col-md-2">Tanggal Penangkapan</label>
                                        <div class="col-md-10">
                                            
                                            <div class='input-group date'>
                                                <input type='text' id="dob_appl2" value="<?= isset($row) ? $row->tgl_penangkapan : ''  ?>" required name="tgl_penangkapan" class="form-control"/>
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                </div>
                                <div class="form-group">
                                        <label for="first_appl" class="col-md-2">Vonis Hukuman</label>
                                        <div class="col-md-10">
                                            <input class="form-control" value="<?= isset($row) ? $row->vonis_hukuman : ''  ?>""  required name="vonis_hukuman" id="first_appl" type="text">
                                        </div>
                                </div>
                                 <div class="form-group">
                                        <label for="first_appl" class="col-md-2">Catatan</label>
                                        <div class="col-md-10">
                                             <textarea id="catatan" name="catatan" class="resize_vertical" rows="4"><?= isset($row) ? $row->catatan : ''  ?></textarea>
                                        </div>
                                </div>

                               
                                </div>
                            </div>
                        
                        </fieldset>
                        <div data-row-span="4">
                                <div data-field-span="4">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-paper-plane"></i> <?= isset($row) ? 'Ubah' : 'Submit' ?></button>
                                <a href="<?= site_url('Dashboard/Pencatatan/data_pelaku') ?>"> <button type="button" class="btn btn-default"><i class="fa fa-long-arrow-left"></i> Cancel</button></a>
                               
                                </div>
                        </div>
                        
                    
                    </form>

                </div>
            </div>
             <br><br><br>
         
            <div class="background-overlay"></div>
            </section>


<script type="text/javascript">
    <?php if(isset($row)){
        ?>
   
    $('#country').val('<?php echo $row->kecamatan_tkp; ?>').trigger("change");
     <?php } ?>
</script>
