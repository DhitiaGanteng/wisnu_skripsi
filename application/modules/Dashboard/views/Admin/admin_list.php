<div class="row">
			 <?php if(!is_null($this->session->flashdata('success'))){ ?>

              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fa fa-check"></i> <?= $this->session->flashdata('success') ?></h5>
               		
              </div>
			        
			   <?php  } ?>   
</div>
<div class="row">
                <div class="col-lg-6">
                    <div class="panel ">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="ti-layout-grid3"></i> Daftar User
                            </h3>
                            <span class="pull-right">
                                    <i class="fa fa-fw ti-angle-up clickable"></i>
                                  
                                </span>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="sample_1">
                                    <thead>
                                    <tr>
                                        <th>
                                            Nama
                                        </th>
                                        <th>Username</th>
                                        <th>Role</th>
                                        <th>
                                            
                                        </th>
                                       
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                    foreach ($users as $key => $value) {
                                   
                                     ?>
                                    <tr>
                                        <td><?= $value->nama_lengkap ?></td>
                                        <td><?= $value->username ?></td>
                                        <td><?= $value->description ?></td>
                                        <td>
                                        	<a href="<?= site_url('Dashboard/Auth/admin_list/'.$value->id)  ?>"><button class="btn btn-primary"><i class="fa fa-edit"></i></button></a>
                                        
                                        		<button type="button"  data-id="<?= $value->id ?>" class="btn btn-danger user_hapus"><i class="fa fa-trash"></i></button>
                                        
                                        </td>
                                   
                                    </tr>
                                   <?php  	
                                    } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- href="<?= site_url('Dashboard/Auth/delete_user/'.$value->id) ?>" -->
                <div class="col-md-6">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="fa fa-fw ti-user"></i> <?php echo (isset($user) ? 'Ubah Data User' : 'Buat User Baru') ?>   
                            </h3>
                            <span class="pull-right">
                                <i class="fa fa-fw ti-angle-up clickable"></i>
                               
                            </span>
                        </div>
                        <div class="panel-body">
                         	 <?php if(!is_null($this->session->flashdata('error'))){ ?>

              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fa fa-ban"></i> <?= $this->session->flashdata('error') ?></h5>
               		
              </div>
			         <?php } ?>
                            <?= form_open('Dashboard/Auth/create_user/'.(isset($user) ? $user->id : ''), array('id'=> 'user-validation', 'class'=> ' bv-form', 'novalidate' => 'novalidate')) ?> 
                               
                                  <?php if(isset($user)){
					               ?>
					          	  
                                 <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="control-label">Username</label>
                                            <input type="text" <?= (isset($user) ? 'disabled' : '') ?>  class="form-control" name="username" value="<?= (isset($user) ? $user->username : '') ?>" placeholder="Username" required="">
                                        </div>
                                    </div>
                                </div>
                                 <div class="row">
                                   <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="control-label">Nama Lengkap</label>
                                            <input type="text" class="form-control" name="nama_lengkap" value="<?= (isset($user) ? $user->nama_lengkap : '') ?>"  placeholder="Nama Lengkap" required="">
                                        </div>
                                    </div>
                                </div>
					                <?php  	
					                }else{
					                	?>
					                	 <div class="row">
					             <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="control-label">Nama Lengkap</label>
                                            <input type="text" class="form-control" name="nama_lengkap" value="<?= (isset($user) ? $user->nama_lengkap : '') ?>"  placeholder="Nama Lengkap" required="">
                                        </div>
                                    </div>
                                </div>
                                 <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="control-label">Username</label>
                                            <input type="text" <?= (isset($user) ? 'disabled' : '') ?>  class="form-control" name="username" value="<?= (isset($user) ? $user->username : '') ?>" placeholder="Username" required="">
                                        </div>
                                    </div>
                                </div>
					                	<?php
					                	}  ?>


                               
                                  <?php if(!isset($user)){
					               ?>
					                 <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="control-label">Password</label>
                                            <input type="password" class="form-control" name="password" placeholder="Password" required="">
                                        </div>
                                    </div>
                                </div>
                                 <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="control-label">Ulangi Password</label>
                                            <input type="password" class="form-control" name="ulangi_password" placeholder="Ulangin Password" required="">
                                        </div>
                                    </div>
                                </div>
					                <?php  	
					                }  ?>

                               
								<div class="row">
                                    <div class="col-sm-12">
	                                 <div class="form-group">
					                  <label for="exampleInputPassword1">Role</label>
					                  <select name="role" class="form-control" required="">
					                  	      <?php if($this->ion_auth->is_admin() && isset($user)){ ?>
                                        <option <?= (isset($user) ? ($user->id_group == 1 ? 'selected': 'a' ) : 'b') ?> value="1">Admin</option>
                                        <?php } ?>
					              
					                  	<option  <?= (isset($user) ? ($user->id_group == 2 ? 'selected': 'a' ) : 'b') ?> value="2">User Biasa</option>
					                  </select>
					                </div>
                                </div>
                                </div>

                                    <?php if(isset($user)){
					                 ?>
					         
					                
					                <hr>
					                <button style="float:right;" type="button" class="btn btn-warning" onclick="ubah_password()"><i class="fa fa-edit"></i> Ubah Password</button>
					                <br><br>
					                <div id="ubah_password">
					                <h4><i class="fa fa-warning"></i> Silahkan masukan password baru.</h4> <br>
					                 <div class="form-group">
					                  <label for="exampleInputPassword1">Masukan Password Baru</label>
					                  <input type="password" class="form-control" name="password_baru" id="password" placeholder="Password Baru" required="">
					                </div>

					                </div>
					                <br><br>
					                <?php   
					                  } ?>


                                
                                <div class="row">
                                    
                                    <div class="<?= isset($user) ? 'col-sm-6' : 'col-sm-12' ?>">
                                        <button type="submit" class="btn btn-primary btn-block"><?= isset($user) ? 'Ubah' : 'Daftar' ?></button>

                                    </div>
                                  
                                      <?php if(isset($user)){ ?>
                                        <div class="col-sm-6">
					                	<a href="<?= site_url('Dashboard/Auth/admin_list') ?>" class="btn btn-default btn-block">Batal</a>
					                	 </div>
					                <?php } ?>


                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            </div>

            <script type="text/javascript">
         

					 $('.user_hapus').on('click', function (e) {
					 		var id = $(this).attr('data-id');

					 		console.log(id);
					        swal({
					            title: 'Apakah anda yakin menghapus data user ini?',
					            text: "",
					            type: 'question',
					            showCancelButton: true,
					            confirmButtonColor: '#66cc99',
					            cancelButtonColor: '#ff6666',
					            confirmButtonText: 'Ya, Hapus User ini.',
					            cancelButtonText: 'Tidak, Batalkan!',
					            confirmButtonClass: 'btn btn-success',
					            cancelButtonClass: 'btn btn-danger'
					        }).then(function () {
					        		
					            swal(
					                'Berhasil Menghapus User',
					                'User Telah berhasil dihapus.',
					                'success'
					            ).then(function(){
					            	var url = '<?= site_url()?>Dashboard/Auth/delete_user/'+id;
									location.href= url;
					            });
					        }, function (dismiss) {
					            // dismiss can be 'cancel', 'overlay',
					            // 'close', and 'timer'
					            if (dismiss === 'cancel') {
					                swal(
					                    'Dibatalakan.',
					                    'Penghapusan User telah dibatalkan.',
					                    'error'
					                );
					            }
					        })
					    });

					 $('#ubah_password').hide();
					  var ubah_password = function(){
					    $('#ubah_password').toggle('slow');
					  }
            </script>
