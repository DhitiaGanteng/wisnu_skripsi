<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 login-form">
            <div class="panel-header">
            <h3 class="text-center">
                 Sistem Informasi Monitoring Tindak Pidana Narkoba
            </h3>
                <h2 class="text-center">

                    <img src="<?= site_url('assets/img/lambang_polda.png') ?>" height="130px;" alt="Logo">
                </h2>
            </div>
            <div class="panel-body">
                <div class="row">
                 <div class="panel-heading">
                    <h5 class="text-center"><strong style="color:red;"> <?php if(isset($error)): 
                    echo $error;
                    endif; ?></strong></h5>
                </div>
                    <div class="col-xs-12">
                         <?= form_open('Dashboard/Auth/login/'.md5("login"), array('id'=> 'authentication', 'class'=> 'login_validator')) ?>
                        <!-- <form action="index.html" id="authentication" method="post" class="login_validator"> -->
                            <div class="form-group">
                                <label for="email" class="sr-only"> E-mail</label>
                                <input type="text" class="form-control  form-control-lg" id="username" name="username"
                                       placeholder="Username" required="">
                            </div>
                            <div class="form-group">
                                <label for="password" class="sr-only">Password</label>
                                <input type="password" class="form-control form-control-lg" id="password"
                                       name="password" placeholder="Password" required="">
                            </div>
                      
                            <div class="form-group">
                                <input type="submit" value="Masuk" class="btn btn-primary btn-block"/>
                            </div>
                          
                        </form>
                    </div>
                </div>
            
            </div>
        </div>
    </div>
</div>