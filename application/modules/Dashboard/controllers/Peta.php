<?php

class Peta extends MY_Controller {

	private $url = "Dashboard/Auth/login";

    public function __construct() {
        parent::__construct();
        $this->load->model(array('ion_auth_model','crud_m'));
       
    }



    public function narkoba($id=null){
    	if($this->ion_auth->logged_in() == FALSE){ 
	          redirect($this->url);
	     }


	    if($this->ion_auth->is_admin() || $this->ion_auth->in_group(2)){

		    if($id){
		    	$data['user'] = $this->crud_m->getAllUsersById($id);
		    }

		
		    $data['header'] = 'Peta Kejahatan Tindak Pidana Narkoba Polres Klaten';
		    $data['subview'] = 'Dashboard/peta/narkoba';
		    $data['peta_kejahatan'] = 'active';
		    $data['data_peta'] = $this->getData();
		    // $data['users'] = $this->crud_m->getAllUsers();

			$this->load_view($data);
		}else{
			redirect('Dashboard/Auth');
		}

    }

    public function getData(){
    	if($this->ion_auth->logged_in() == FALSE){ 
	          redirect($this->url);
	     }

	     $data = $this->crud_m->getMapData();
	     $temp = [];
	     foreach ($data as $key => $row) {
	     	array_push($temp, array("name" => $row->name, "value" => $row->value));
	     }

	 
	    return json_encode($temp);
    }


   

    

}

// 081274082905