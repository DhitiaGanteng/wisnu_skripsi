<?php

class Pencatatan extends MY_Controller {

	private $url = "Dashboard/Auth/login";

    public function __construct() {
        parent::__construct();
        $this->load->model(array('ion_auth_model','crud_m'));
       
    }



    public function data_pelaku($id=null){
    	if($this->ion_auth->logged_in() == FALSE){ 
	          redirect($this->url);
	     }


	    if($this->ion_auth->is_admin() || $this->ion_auth->in_group(2)){


		
		    $data['header'] = 'Daftar Semua Pelaku Tindak Pidana Narkoba <b>Polres Klaten</b>';
		    $data['subview'] = 'Dashboard/Pencatatan/daftar_pelaku';
		    $data['pelaku'] = 'active';
		    $data['list_pelaku'] = 'active';
		    $data['pelaku_data'] = $this->crud_m->get_all('data_pelaku');

			$this->load_view($data);
		}else{
			redirect('Dashboard/Auth');
		}

    }

    public function form_pelaku($id=null){
    	if($this->ion_auth->logged_in() == FALSE){ 
	          redirect($this->url);
	     }


	    if($this->ion_auth->is_admin()){
	    	if(isset($id)){
	    		$data['row'] = $this->crud_m->get_by($id,'id','data_pelaku');
	    	}
		
		    $data['header'] = 'Form Data Pelaku Tindak Pidana Narkoba';
		    $data['subview'] = 'Dashboard/Pencatatan/form_pelaku.php';
		    $data['pelaku'] = 'active';
		    $data['form_pelaku'] = 'active';
		    $data['users'] = $this->crud_m->getAllUsers();

			$this->load_view($data);
		}else{
			redirect('Dashboard/Auth');
		}

    }

    public function proses_form($id=null){
    	$nama_lengkap = $this->input->post('nama_lengkap');
    	$jenis_kelamin = $this->input->post('jenis_kelamin');
    	$tempat_lahir = $this->input->post('tempat_lahir');
    	$tgl_lahir = $this->input->post('tanggal_lahir');
    	$alamat = $this->input->post('alamat');
    	$alamat_tkp = $this->input->post('alamat_tkp');
    	$kecamatan_tkp = $this->input->post('kecamatan_tkp');
    	$tgl_penangkapan = $this->input->post('tgl_penangkapan');
    	$vonis_hukuman = $this->input->post('vonis_hukuman');
    	$catatan = $this->input->post('catatan');


    	$data = array(
    		'nama_lengkap' => $nama_lengkap,
    		'jenis_kelamin' => $jenis_kelamin,
    		'tempat_lahir' => $tempat_lahir,
    		'tanggal_lahir' => $tgl_lahir,
    		'alamat' => $alamat,
    		'alamat_tkp' => $alamat_tkp,
    		'kecamatan_tkp' => $kecamatan_tkp,
    		'tgl_penangkapan' => $tgl_penangkapan,
    		'vonis_hukuman' => $vonis_hukuman,
    		'catatan' => $catatan,
    		);

    
    	if($_FILES['foto']['name'] != ''){
    		$data = array_merge($data, array('foto' => str_replace(' ', '_', $_FILES['foto']['name'])));
    		$upload = $this->crud_m->yuk_upload('foto');

    	}


    	if(isset($id)){
    		$data = array_merge($data, array('id' => $this->input->post('id')));
    		$update = $this->crud_m->update('data_pelaku', $data, 'id');
    		if($update){
    			$this->session->set_flashdata('success', 'Berhasil Mengubah Data Pelaku.');
				redirect('Dashboard/Pencatatan/data_pelaku');
    		}
    	}else{
    		$insert = $this->crud_m->insert('data_pelaku', $data);
	    	if($insert){
	    		$this->session->set_flashdata('success', 'Berhasil Menambahkan Data Pelaku Baru.');
				redirect('Dashboard/Pencatatan/data_pelaku');
	    	}
    	}
    	
    	

    }

    public function detail_pelaku($id){
    	if($this->ion_auth->logged_in() == FALSE){ 
	          redirect($this->url);
	     }


	    if($this->ion_auth->is_admin() || $this->ion_auth->in_group(2)){

		
		    $data['header'] = 'Detail Informasi Pelaku Tindak Pidana Narkoba';
		    $data['subview'] = 'Dashboard/Pencatatan/detail_pelaku.php';
		    $data['pelaku'] = 'active';
		    $data['data_pelaku'] = 'active';
		    $data['row_pelaku'] = $this->crud_m->get_by($id,'id','data_pelaku');

			$this->load_view($data);
		}else{
			redirect('Dashboard/Auth');
		}
    }

    public function delete_pelaku($id){
    	$this->crud_m->delete('data_pelaku','id',$id);
    	$this->session->set_flashdata('success', 'Berhasil Menghapus Pelaku Pidana.');
    		redirect('Dashboard/Pencatatan/data_pelaku');

    }

   

    

}

