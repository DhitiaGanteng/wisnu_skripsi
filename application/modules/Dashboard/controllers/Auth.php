<?php

class Auth extends MY_Controller {

	private $url = "Dashboard/Auth/login";

    public function __construct() {
        parent::__construct();
        $this->load->model(array('ion_auth_model','crud_m'));
       
    }


    public function index(){

	    if($this->ion_auth->logged_in() == FALSE){ 
	          redirect($this->url);
	     }

			$data = array(
				'title' =>'Selamat Datang di Sistem Informasi Monitoring Tindak Pidana Narkoba',
				'header' => 'Selamat Datang di Sistem Informasi Tindak Pidana Narkoba Polres Klaten',
				'subview'=>'Dashboard/dashboard',
				'dashboard' => 'active',
				'total_pelaku' => $this->crud_m->total_pelaku()
				);
		
			$this->load_view($data);

    }

    public function admin_list($id=null){
    	if($this->ion_auth->logged_in() == FALSE){ 
	          redirect($this->url);
	     }


	    if($this->ion_auth->is_admin()){

		    if($id){
		    	$data['user'] = $this->crud_m->getAllUsersById($id);
		    }

			// $this->breadcrumbs->push('Admin Lists', '/');
		    $data['header'] = 'Daftar Semua User';
		    $data['subview'] = 'Dashboard/Admin/admin_list';
		    $data['admin'] = 'active';
		    $data['users'] = $this->crud_m->getAllUsers();

			$this->load_view($data);
		}else{
			redirect('_Admin/admin');
		}

    }


    public function create_user($id=null){
    	if($this->ion_auth->logged_in() == FALSE){ 
	          redirect($this->url);
	     }

	    if($this->ion_auth->is_admin()){


    	if($id){

    		$data = array(
						'nama_lengkap' => $this->input->post('nama_lengkap')
						 );

    		if($this->input->post('password_baru') != ''){
    			$data = array(
						'nama_lengkap' => $this->input->post('nama_lengkap'),
						'password' => $this->input->post('password_baru')
						 );
    		}
    		

    	
			$this->ion_auth->update($id, $data);
			$update = $this->crud_m->updateGroup($id, $this->input->post('role'));

			if($update){
				$this->session->set_flashdata('success', 'Berhasil Mengubah Data User.');
				redirect('Dashboard/Auth/admin_list');
			}


	
    	}else{

	    	$nama_lengkap = $this->input->post('nama_lengkap');
	    	$username = $this->input->post('username');
	    	$password = $this->input->post('password');
	    	$ulangin_password = $this->input->post('ulangi_password');

			$additional_data = array(
								'nama_lengkap' => $nama_lengkap
								);
			$group = array($this->input->post('role')); // Sets user to admin.
			if(!$this->ion_auth->username_check($username)){

				$this->ion_auth->register($username, $password, $username, $additional_data, $group);
				$this->session->set_flashdata('success', 'Berhasil Menambahkan User Baru.');
				redirect('Dashboard/Auth/admin_list');

			}else{
				$this->session->set_flashdata('error', 'Oops! User Sudah Terdaftar.');
				redirect('Dashboard/Auth/admin_list');
			}

			
    	}
      }else{
      	redirect('Dashboard/Auth');
      }

    }

    public function delete_user($id=null){
    	if($this->ion_auth->logged_in() == FALSE){ 
	          redirect($this->url);
	     }

	     if($this->ion_auth->is_admin()){

    		$this->ion_auth->delete_user($id);
    	
    		$this->session->set_flashdata('success', 'Berhasil Menghapus User.');
    		redirect('Dashboard/Auth/admin_list');

    	}else{
    		redirect('_Admin/admin');
    	}
    	
    }

    public function login($params=null){
    	if($this->ion_auth->logged_in() == FALSE){
		
			switch($params):
				case md5("login"):

					 $login_validation = array(
						array('field' => 'username', 'label' => 'username','rules' => 'required|trim'),
						array('field' => 'password', 'label' => 'Password', 'rules' => 'required|trim')	
					 );
					$this->form_validation->set_rules($login_validation);
      			  	if($this->form_validation->run() == true){

		            	if($this->ion_auth_model->login($this->input->post('username'),$this->input->post('password'))){

		            			redirect('');
		        		
		                }else{
		                    $this->data['error'] = "Username / Password Salah !";
		                }
           			}
				default:
			 		$this->load_view_login('Dashboard/login');
				break;
					$this->load_view_login('Dashboard/login');
			endswitch;
		
           
        }
    }

    public function logout(){
    	if($this->ion_auth->logged_in() == FALSE){ 
	          redirect($this->url);
	     }
    	$this->ion_auth->logout();
		redirect('Dashboard/Auth');
    }



    public function change_password(){
    	$data = array(
				'title' =>'Change Password',
				'subview'=>'_Admin/Admin/change_password',
				'dashboard' => 'active'
				);
    	$this->load_view($data);
    }

    public function change_password_process(){
    	$data = array('password' => $this->input->post('password_baru'));
		$update = $this->ion_auth->update($id, $data);

			if($update){
				$this->session->set_flashdata('success', 'Successfully, Updating your password.');
				redirect('_Admin/admin/change_password');
			}
  
    }

    

}

// 081274082905